
### What is this repository for? ###

This work is supported by 

       National Science Foundation (NSF) DMS - 1521590

This code is to solve Optimization Problems with PDE Constraints (PDECO). Even though the 
approach considered here is general but we focus on PDECO with semilinear elliptic PDE 
constraints. We call this problem as unconstrained problem. We use Newton-CG or LBFGS to
solve this problem. For Newton-CG and LBFGS we using the implementaiton by M. Heinkenschloss

       http://www.caam.rice.edu/~heinken/papers/matlab_impl_constr/index.html

If the control constraints are present we call that problem as control constrained PDECO. 
We solve this problem using semismooth Newton method.
