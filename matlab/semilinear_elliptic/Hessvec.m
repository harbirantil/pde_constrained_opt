%
% Compute the Hessian vector product 
%

function [ Hv ] = Hessvec( v, z, usr_par) 

global GLB

mesh = GLB.mesh;
FreeNodes = GLB.FreeNodes;
data = GLB.data;

N  = size(mesh.p,1);

if ~isempty(usr_par)   
   u = usr_par{1};  % state 
   p = usr_par{2};  % adjoint
   if length(p) < N  
      % p contains Dirichlet BC
      ptemp = zeros(N,1);
      ptemp(FreeNodes) = p;
      p = ptemp;
   end       
   K = usr_par{3};  % coefficient matrix
end    

%% Step 1: solve the state equation

%% Step 2: solve the adjoint equation

%% Step 3: Solve the aux equation 
rhs = GLB.B0*v;
w = K \ rhs;   

%% Step 4: Solve another aux equation
[~,~,DDNewt] = nonlin_assemb(mesh,u,data,p);
rhs = GLB.M*w-DDNewt(FreeNodes,FreeNodes)*w;
zeta = K'\rhs;

%% Step 6: Compute the Hessian vector product
Hv = (GLB.B0'*zeta + GLB.lambda*GLB.M0*v); % Hessian-vec product in dual space

%ell = diag(M0); S = sparse(1:NT,1:NT,1./ell,NT,NT);
Hv = GLB.M0 \ Hv; % multiplication by S brings Hessian-vector product to primal space

