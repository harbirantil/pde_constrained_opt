%
% Evaluate the gradient of objective function
%

function [ fg ] = grad( z, usr_par )

global GLB

if ~isempty(usr_par)
   p = usr_par{2};  % set the adjoint variable 
end    

fg = (GLB.B0'*p + GLB.lambda*GLB.M0*z);   % gradient in the dual space

%ell = diag(M0); S = sparse(1:NT,1:NT,1./ell,NT,NT);
fg = GLB.M0 \ fg; % multiplication by S brings the gradient to primal space
