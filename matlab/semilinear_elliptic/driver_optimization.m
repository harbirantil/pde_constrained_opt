%
% Solve an optimal control problem with a semilinear PDE
% constraints 
%
%    
%   min J(u,z) := 1/2 ||u-ud||_{L^2(\Om)}^2 + 1/2 ||z||_{L^2(\Om)}^2
%
%   subject to 
%
%      -\Delta u + u^3 = f + z  in \Om
%                   u  = uD     on \bdy\Omega
%
% Here u is the state and z is the control. In addition one can impose
% constraints on the control variable as well
%
% We also compute the rate of convergence. We discretize
% state and adjoint using piecewise linear FEM and control using
% piecewise constants (or piecewise linears). 
%
% Note that we compute the rate of convergence for consistency.
% Typically one is interested in solving the optimization problem
% and not the rate of convergence. 
% 
%
% Author:  Harbir Antil
%          Department of Mathematical Sciences
%          George Mason University
%          Fairfax VA
%        
% Date Created: January 12, 2018
% 


clear all
close all

addpath ../FEM/
addpath ../OPT/

set(0, 'defaultaxesfontsize',16,'defaultaxeslinewidth',1,...
      'defaultlinelinewidth',1,'defaultpatchlinewidth',0.7,...
      'defaulttextfontsize',18);
  
global GLB
  
data = exact_soln_ctrl;  

lambda = 0.01;

NR = 6;
errL2_state = zeros(NR,1);
errL2_ctrl  = zeros(NR,1);
Ndof  = zeros(NR,1);
usr_par = [];

GLB.lambda = lambda;

% UNCONSTRAINED CASE: optimizer = 1 (newton-cg), 2 (LBFGS). 
% Set  GLB.ctrl = 1
% CONSTRAINED CASE: optimizer = 3 
% Set  GLB.ctrl = 2
 
optimizer = 1;        
if (optimizer == 1) || (optimizer == 2)
   GLB.ctrl  = 1;      % unconstrained case               
elseif (optimizer == 3)
   GLB.ctrl  = 2;      % constrained case
end   
    
GLB.ctrl_disc = 'pw_constant';  % pw_linear for piecewise linear discretiztaion
                               %    of control
                               % pw_constant for piecewise constant discretiztaion
                               %    of control

for ind = 1:NR
    % Generate the mesh
    mesh = rect_grid2(0, 1, 0, 1, 2^(ind+1), 2^(ind+1));
    
    NT = size(mesh.t,1);
    N  = size(mesh.p,1);
    
    GLB.mesh      = mesh;    
    if GLB.ctrl == 2          % control constrained case
       GLB.za = 0;
       GLB.zb = 7; 
    end   
    
    % assemble matrices
    [A,M,B0,M0,FreeNodes,dirichlet] = Stiff_Mass(GLB.mesh);    
    
    % add Diricichlet conditions
    uD = zeros(N,1);
    uD(dirichlet) = data.exactu(GLB.mesh.p(dirichlet,:));
    b = M(FreeNodes,:)*data.f(GLB.mesh.p,GLB.lambda)-A(FreeNodes,:)*uD;
    
    GLB.A = A(FreeNodes,FreeNodes);
    GLB.M = M(FreeNodes,FreeNodes);    
    GLB.FreeNodes = FreeNodes;
    GLB.dirichlet = dirichlet;    
    GLB.uD        = uD;
    GLB.b         = b;
    GLB.c         = data.udesired(mesh.p(FreeNodes,:));
    GLB.data      = data;
    GLB.hess      = 'hess';
    
    if (strcmp(GLB.ctrl_disc,'pw_constant'))
       GLB.B0 = B0(FreeNodes,:);
       GLB.M0 = M0;                 
    elseif (strcmp(GLB.ctrl_disc,'pw_linear'))   
       GLB.B0 = GLB.M;
       GLB.M0 = GLB.M;
    end
    
    clear A M B0 M0 FreeNodes dirichlet lambda uD
    
    % perform derivative checks
    if (strcmp(GLB.ctrl_disc,'pw_constant'))
       z = zeros(NT,1);
    elseif (strcmp(GLB.ctrl_disc,'pw_linear'))   
       z = zeros(length(GLB.FreeNodes),1);
    end   
    if (0)
       deriv_check( z, 1, usr_par);
    end   
        
    if( optimizer == 1) % unconsrained case
        fprintf(1,' Newton''s method\n')
        % set options for Newton CG method
        options.iprint = 1; % print level, default = 0
        options.maxit  = 20; %  maximum number of iterations, default = 100
        options.gtol   = 1.e-8; % gradient stopping tolerance, default = 1.e-8
        options.stol   = 1.e-8; % stepsize stopping tolerance, default = 1.e-8
        
        [ z, iter, iflag ]  = newton_cg( z, options, usr_par);
        fprintf(1,' Newton''s method returned after %d iterations with flag %d \n', iter,iflag )
        
    elseif( optimizer == 2) % unconsrained case
        fprintf(1,' limited memory BFGS method\n')
        % set options for limited memory BFGS method
        options.iprint = 1; % print level, default = 0
        options.gtol   = 1.e-8; % gradient stopping tolerance, default = 1.e-8
        options.stol   = 1.e-8; % stepsize stopping tolerance, default = 1.e-8
        options.maxit  = 50; % Maximum number of iterations, default 100
        options.L      = 20;  % LBFGS storage limit, default 20
        [ z, iter, iflag ]  = lbfgs( z, options, usr_par);
        fprintf(1,' LBFGS returned after %d iterations with flag %d \n', iter,iflag )
        
    elseif( optimizer == 3) % constrained case
        fprintf(1,' Semi-smooth Newton method method\n')
        options.iprint = 1; % print level, default = 0
        options.gtol   = 1.e-8; % gradient stopping tolerance, default = 1.e-8
        options.maxit  = 50; % Maximum number of iterations, default 100        
        [z, iter, iflag] = semismooth(z, options, usr_par);
        fprintf(1,' Semismooth returned after %d iterations with flag %d \n', iter,iflag )
        
    else     
        fprintf(1,' optimizer is %d; must be 1, 2 \n', optimizer)
    end
    
    %% plot the solution
    [usr_par] = xnew(z,[]);
    u = usr_par{1};
    p = usr_par{2};
    
    % plot desired state
    figure(1); clf
    set(gcf,'Units','normal');
    set(gcf,'Position',[0.25,0.25,0.6,0.25]);
    subplot(1,4,1)
    trisurf(mesh.t,mesh.p(:,1),mesh.p(:,2),data.udesired(mesh.p))
    title('desired state')
    axis tight; %axis equal
    
    % plot optimal state
    subplot(1,4,2)
    trisurf(mesh.t,mesh.p(:,1),mesh.p(:,2),u)
    title('optimal state')
    axis tight; %axis equal
    
    % plot control
    subplot(1,4,3)
    if (strcmp(GLB.ctrl_disc,'pw_constant'))
        elemnew = reshape(1:3*NT,NT,3);
        nodenew = mesh.p(mesh.t(:),:);
        unew = repmat(z,3,1);
        h = trisurf(elemnew, nodenew(:,1), nodenew(:,2), unew', ...
            'FaceColor', 'interp', 'EdgeColor', 'interp');
    elseif (strcmp(GLB.ctrl_disc,'pw_linear'))
        zt = zeros(N,1); zt(GLB.FreeNodes) = z;
        trisurf(mesh.t,mesh.p(:,1),mesh.p(:,2),zt)
    end
    title('optimal control')
    axis tight
    
    % plot adjoint
    pt = zeros(N,1); pt(GLB.FreeNodes) = p;
    subplot(1,4,4)
    trisurf(mesh.t,mesh.p(:,1),mesh.p(:,2),pt)
    title('optimal adjoint')
    axis tight; %axis equal     
    drawnow
    
    % L2-error state
    u_uexact = u(GLB.FreeNodes)-data.exactu(mesh.p(GLB.FreeNodes,:));
    errL2_state(ind,1) = sqrt( u_uexact'*GLB.M*u_uexact );
    Ndof(ind,1) = N;
    
    % L2-error control
    if GLB.ctrl == 1
        exact = data.exactz(mesh.p(GLB.FreeNodes,:),GLB.lambda)'*...
                GLB.M*data.exactz(mesh.p(GLB.FreeNodes,:),GLB.lambda);
        exact = exact - 2*data.exactz(mesh.p(GLB.FreeNodes,:),GLB.lambda)'*GLB.B0*z;
    elseif GLB.ctrl == 2   
        exact = data.exactz_constrained(mesh.p(GLB.FreeNodes,:),GLB.lambda,GLB.za,GLB.zb)'*...
                GLB.M*data.exactz_constrained(mesh.p(GLB.FreeNodes,:),GLB.lambda,GLB.za,GLB.zb);
        exact = exact - 2*data.exactz_constrained(mesh.p(GLB.FreeNodes,:),GLB.lambda,GLB.za,GLB.zb)'*GLB.B0*z; 
    end
    errL2_ctrl(ind,1) = sqrt( exact + z'*GLB.M0*z );
end    

figure(2); clf   
% reference line for state
k = 2;
p = polyfit(log(Ndof(k:end)),log(errL2_state(k:end)),1);
r = single(p(1));
s = 0.75*errL2_state(1)/Ndof(1)^r;
loglog(Ndof,s*Ndof.^r,'k-.','linewidth',1)
hold on

% error plot for state
loglog(Ndof,errL2_state,'-*','linewidth',2);

title(['Rate of convergence is CN^{' num2str(r,2) '}'],'FontSize', 14);
h_legend = legend('L^2-error state',['CN^{' num2str(r,2) '}'],'LOCATION','Best');
set(h_legend,'FontSize', 14);
xlabel('number fo unknowns')
ylabel('error')
axis tight
hold off

figure(3); clf
% reference line for control
k = 2;
p = polyfit(log(Ndof(k:end)),log(errL2_ctrl(k:end)),1);
r = single(p(1));
s = 0.75*errL2_ctrl(k)/Ndof(1)^r;
loglog(Ndof,s*Ndof.^r,'k-.','linewidth',1)
hold on

% error plot for control
loglog(Ndof,errL2_ctrl,'-*','linewidth',2);

title(['Rate of convergence is CN^{' num2str(r,2) '}'],'FontSize', 14);
h_legend = legend('L^2-error control',['CN^{' num2str(r,2) '}'],'LOCATION','Best');
set(h_legend,'FontSize', 14);
xlabel('number fo unknowns')
ylabel('error')
axis tight
