% assemble nonlinear terms
function [DNewt, Newt, DDNewt] = nonlin_assemb(mesh,uh,data,ph)

elem = mesh.t;
node = mesh.p;

NT = size(elem,1);
N  = size(node,1);
Ndof = N;

%% Compute geometric quantities and gradient of local basis
[~,area] = gradbasis(mesh.p,mesh.t);  % using the code from iFEM by L. Chen

%% quadrature points and weights
[lambda,weight] = quadpts(7);
nQuad = size(lambda,1);


%% assemble nonlinearities
fn  = zeros(NT,3);
Dfn = zeros(NT,3,3);
if nargout > 2
   DDfn = zeros(NT,3,3);
end
for p = 1:nQuad    
    % evaluate uh at quadrature point
    uhp = uh(elem(:,1))*lambda(p,1) + ...
          uh(elem(:,2))*lambda(p,2) + ...
          uh(elem(:,3))*lambda(p,3);  
    [non,dnon] = data.nonlin(uhp);  
    if nargout > 2
       php = ph(elem(:,1))*lambda(p,1) + ...
             ph(elem(:,2))*lambda(p,2) + ...
             ph(elem(:,3))*lambda(p,3);   
       [~,~,ddnon] = data.nonlin(uhp);   
    end    
    for i = 1:3
        for j = 1:3
            Dfn(:,i,j) = Dfn(:,i,j) + area.*weight(p).*dnon.*lambda(p,j).*lambda(p,i);
            if nargout > 2
               DDfn(:,i,j) = DDfn(:,i,j) + area.*weight(p).*ddnon.*php.*lambda(p,j).*lambda(p,i); 
            end    
        end
        fn(:,i) = fn(:,i) + area.*weight(p).*(non)*lambda(p,i);
    end
end
        
Newt = zeros(Ndof,1);
DNewt = sparse(N,N);        
DDNewt = sparse(N,N);        
for i = 1:3
    krow = elem(:,i);
    for j = 1:3
        kcol = elem(:,j);
        DNewt = DNewt + sparse(krow,kcol,Dfn(:,i,j),N,N);                      
        if nargout > 2
           DDNewt = DDNewt + sparse(krow,kcol,DDfn(:,i,j),N,N);                       
        end    
    end 
end 
Newt   = Newt + accumarray(elem(:),fn(:),[Ndof,1]); 
