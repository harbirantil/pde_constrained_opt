% compute the data and nonlinearities (notice that here we are
% given exact solution
% 
% Author:  Harbir Antil
%          Department of Mathematical Sciences
%          George Mason University
%          Fairfax VA
%        
% Date Created: January 12, 2018
%

function data = exact_soln_ctrl

global GLB

data = struct('f',@f,'exactu',@exactu,'nonlin',@nonlin,...
       'udesired',@udesired,'exactz',@exactz,'exactp',@exactp,...
       'exactz_constrained',@exactz_constrained);

% exact solution u
    function u =  exactu(p)
        u = sin(2*pi*p(:,1)).*sin(2*pi*p(:,2));
    end

% right hand side
    function f =  f(p,lambda)
        f = 8*pi^2*(sin(2*pi*p(:,1)).*sin(2*pi*p(:,2))) + exactu(p).^3;
        if GLB.ctrl == 1
            f = f - exactz(p,lambda);
        elseif GLB.ctrl == 2
            f = f - exactz_constrained(p,lambda,GLB.za,GLB.zb);
        end    
    end

% ud
    function [u] = udesired(p)
        u  = exactu(p)-(8*pi^2+3*exactu(p).^2).*exactp(p);
    end

% p
    function [u] = exactp(p)
        u  = exactu(p);
    end

% z (unconstrained case)
    function [u] = exactz(p,lambda)
        u  = -exactp(p)/lambda;
    end

% z (constrained case)
    function [u] = exactz_constrained(p,lambda,a,b)
        u  = min(b,max(-exactp(p)/lambda,a));
    end

% nonlinear function
    function [u,du,duu] = nonlin(x)
        u  = x.^3;
        du = 3*x.^2;        
        if nargout > 2
           duu = 6*x; 
        end    
    end

end

