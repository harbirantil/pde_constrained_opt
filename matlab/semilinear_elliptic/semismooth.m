%
% Semismooth Newton method to solve the optimal control problem with
% inequality constraints
%
% Author:  Harbir Antil
%          Department of Mathematical Sciences
%          George Mason University
%          Fairfax VA
%        
% Date Created: January 12, 2018
%

function [x, iter, iflag] = semismooth(x, options, usr_par)

global GLB

mesh = GLB.mesh;
data = GLB.data;
FreeNodes = GLB.FreeNodes;
N = size(mesh.p,1);

%initialization
iflag       = 0;
iter        = 0;

% tolereance and max iterations 
iprint  = 0;
fid     = 1;
maxit   = 100;
gtol    = 1.e-8;
if( ~isempty(options) )
    if( isfield(options,'iprint') ); iprint = options.iprint; end
    if( isfield(options,'fid') );    fid    = options.fid; end
    if( isfield(options,'maxit') );  maxit  = options.maxit; end
    if( isfield(options,'gtol') );   gtol   = options.gtol; end    
end

usr_par  = xnew( x, 0, usr_par);
g        = grad(x, usr_par);
gnorm    = sqrt(xprod( g, g, usr_par));

u = usr_par{1};
p = usr_par{2};
rhs = GLB.b;

% inactive nodes
indinact = 1:length(FreeNodes);

if( iprint > 0 )
   fprintf(1,' semismooth newton \n')
   fprintf(1,'     k    ||gradF(x_k)||    \n');
end

res10 = -(GLB.A*u(FreeNodes) - rhs);
res20 = -(GLB.A*p -GLB.M*(u(FreeNodes)-GLB.c));
nres10 = min(sqrt(res10'*GLB.M*res10),sqrt(res20'*GLB.M*res20));

%stopping criteria for semismooth newton
while ( ~(gnorm < gtol ) & iter <= maxit & iflag == 0 ) 
    
   pt = zeros(N,1); pt(FreeNodes) = p; 
   [DNewt,Newt,DDNewt] = nonlin_assemb(mesh,u,data,pt);   
   scal = (GLB.lambda*GLB.M0) \ (GLB.B0'*p);
   temp = -scal-max(-scal-GLB.zb,0)+max(GLB.za+scal,0);
   res1 = -(GLB.A*u(FreeNodes)+Newt(FreeNodes) - GLB.B0*temp - rhs);
   res2 = -(GLB.A*p + DNewt(FreeNodes,FreeNodes)*p-GLB.M*(u(FreeNodes)-GLB.c));   
   gnorm = max( sqrt(res1'*GLB.M*res1), sqrt(res2'*GLB.M*res2))/nres10;
   
   if( iprint > 0 )
      fprintf(fid,'  %4d  %12.6e \n',iter, gnorm );
   end
   
   if gnorm < gtol
      break 
   end    
      
   M0 = GLB.lambda*GLB.M0(indinact,indinact);
   lM0 = size(M0,1);
   ell = diag(M0); S = sparse(1:lM0,1:lM0,1./ell,lM0,lM0);
               
   K = [ GLB.A+DNewt(FreeNodes,FreeNodes)  GLB.B0(:,indinact)*S*GLB.B0(:,indinact)'
        DDNewt(FreeNodes,FreeNodes)-GLB.M  GLB.A+DNewt(FreeNodes,FreeNodes)];
              
   du = K \ [res1; res2]; 
   u(FreeNodes)  = u(FreeNodes)+du(1:length(FreeNodes));  % may require line search
   p = p + du(length(FreeNodes)+1:2*length(FreeNodes));
   
   % active set
   indactup =  GLB.za + ((GLB.lambda*GLB.M0) \ (GLB.B0'*p)) > 0;  
   indactdn = -GLB.zb - ((GLB.lambda*GLB.M0) \ (GLB.B0'*p)) > 0;
   act = (indactup | indactdn);
   % inactive set
   indinact = ~(act); 
   
   iter = iter+1;
end    
x = temp; 

if ( gnorm > gtol & iter >= maxit)
   iflag = 3;
end

