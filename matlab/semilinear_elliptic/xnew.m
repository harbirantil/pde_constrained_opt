% generate the solution to the state and adjoint equations
%

function [ usr_par ] = xnew( z, iter, usr_par )

global GLB

mesh = GLB.mesh;
data = GLB.data;
FreeNodes = GLB.FreeNodes;

u = GLB.uD;
rhs = GLB.b + GLB.B0*z;

New_iter = 100;
New_tol  = 1e-10;
res0 = rhs - (GLB.A*u(FreeNodes));
L2normres0 = sqrt(res0'*GLB.M*res0);

% solve state equation
for ell_1 = 1:New_iter    
    % assemble nonlinearites 
    [DNewt,Newt] = nonlin_assemb(mesh,u,data);
    res = -(GLB.A*u(FreeNodes)+Newt(FreeNodes) - rhs);
    L2normres = sqrt(res'*GLB.M*res);
    %fprintf('rel error in L2-norm %12.4e \n',L2normres/L2normres0)
    if L2normres/L2normres0 < New_tol
       break 
    end    
    K  = GLB.A + DNewt(FreeNodes,FreeNodes);
    du = K \ res;
    u(FreeNodes) = u(FreeNodes) + du;         
end
%

%% adjoint equation
rhs = GLB.M*(u(FreeNodes)-GLB.c); 
p = K' \ rhs;         

usr_par{1} = u;
usr_par{2} = p;
if strcmp(GLB.hess,'hess')
   usr_par{3} = K;
end   

