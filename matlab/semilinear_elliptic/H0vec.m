% 
% compute H0*v where H0 is the initial BFGS matrix 
% H0 is a replacement of the inverse of the Hessian of f
%

function [Hv] = H0vec(v, usr_par)

global GLB

% use lambda*I as initial Hessian approximation 
Hv = v/GLB.lambda;

% use lambda*M0 as initial Hessian approximation
%Hv = GLB.lambda\(GLB.M0\v);
