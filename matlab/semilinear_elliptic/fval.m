%
% Evaluate the objective function:
%
%   J(u,z) := 1/2 ||u-ud||_{L^2(\Om)}^2 + 1/2 ||z||_{L^2(\Om)}^2
%
% Author:  Harbir Antil
%          Department of Mathematical Sciences
%          George Mason University
%          Fairfax VA
%        
% Date Created: January 12, 2018
%

function [ f ] = fval(z, usr_par)

global GLB

if ~isempty(usr_par)
   u = usr_par{1}; 
end    
    
u_ud = u(GLB.FreeNodes) - GLB.c;

f = u_ud'*GLB.M*u_ud + GLB.lambda*z'*GLB.M0*z;
f = 0.5*f;
