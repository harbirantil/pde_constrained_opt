function [x1x2] = xprod( x1, x2, usr_par)
%
% Version June 6, 2008
% Matthias Heinkenschloss
%

% Modified by H. Antil to include M0

global GLB

% Euclidean inner product
x1x2 = x1'*GLB.M0*x2; 

% End of xprod
